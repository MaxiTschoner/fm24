# Testing of machine learning models
- Create models of machine learning according to [kaggle notebook](https://www.kaggle.com/code/tanmay111999/heart-failure-prediction-cv-score-90-5-models). The model_factory.ipynb is a simplified version of the kaggle notebook.
- Store them in the models folder
- Use the assigned docker-compose file to run the models
- The models will be tested with sklearn for accuracy, precision, recall, ROC-AUC, and CVS
- The results will be stored in the results folder

### How to run the models
- Clone the repository
- Run the docker-compose file
- The models will be tested and the results will be stored in the results folder

### How to run the docker-compose file
- Edit the enviorment variables in the docker-compose file to the desired model
- Run the following command in the terminal
```bash
docker-compose up
```
- If the main.py is changed, you have to rebuild the docker image
```bash
docker-compose up --build
```


### Results
- The results will be stored in the results folder
- The results will be stored in the form of a json file
- The results will contain the accuracy, precision, recall, ROC-AUC, and CVS of the models

### Reproducibility
- The models can be reproduced by using the model_factory.ipynb file
- You have to install the required libraries to run the model_factory.ipynb file

Install the required libraries by running the following command
```bash
pip install -r requirements.txt
```
