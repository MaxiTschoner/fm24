import os
import json
import sys
import pandas as pd
import joblib
from datetime import datetime
from sklearn.metrics import accuracy_score, precision_score, recall_score, roc_auc_score
from sklearn.model_selection import cross_val_score

# Load the model name from environment
model_name = os.getenv('MODEL_NAME')
if not model_name:
    print("Environment variable MODEL_NAME not set.")
    sys.exit(1)

model_path = f"/mounted/models/{model_name}.joblib"
if not os.path.exists(model_path):
    print(f"No model found at {model_path}")
    sys.exit(1)

# Load the model
model = joblib.load(model_path)

# Load data
data = pd.read_csv("/app/data/heart.csv")

# Check if the expected target column exists
if 'HeartDisease' not in data.columns:
    print("Expected 'HeartDisease' column not found in dataset.")
    sys.exit(1)

from sklearn.preprocessing import LabelEncoder, MinMaxScaler, StandardScaler
le = LabelEncoder()
mms = MinMaxScaler()  # Normalization
ss = StandardScaler()  # Standardization

# Encoding categorical data
data['Sex'] = le.fit_transform(data['Sex'])
data['ChestPainType'] = le.fit_transform(data['ChestPainType'])
data['RestingECG'] = le.fit_transform(data['RestingECG'])
data['ExerciseAngina'] = le.fit_transform(data['ExerciseAngina'])
data['ST_Slope'] = le.fit_transform(data['ST_Slope'])

# Scaling numerical data
data['Oldpeak'] = mms.fit_transform(data[['Oldpeak']])
data['Age'] = ss.fit_transform(data[['Age']])
data['RestingBP'] = ss.fit_transform(data[['RestingBP']])
data['Cholesterol'] = ss.fit_transform(data[['Cholesterol']])
data['MaxHR'] = ss.fit_transform(data[['MaxHR']])

X = data[data.columns.drop(['HeartDisease','RestingBP','RestingECG'])].values
y = data['HeartDisease'].values

# Make predictions
predictions = model.predict(X)
try:
    probabilities = model.predict_proba(X)[:, 1]
except AttributeError:
    print("Probability predictions are not available for this model.")
    probabilities = None  # Or handle it in another suitable way


# Calculate metrics
accuracy = accuracy_score(y, predictions)
precision = precision_score(y, predictions)
recall = recall_score(y, predictions)
roc_auc = roc_auc_score(y, probabilities) if probabilities is not None else None
cvs = cross_val_score(model, X, y, cv=5).mean()

# Compile all metrics
metrics = {
    "accuracy": accuracy,
    "precision": precision,
    "recall": recall,
    "ROC_AUC": roc_auc if roc_auc is not None else "Not available",
    "CVS": cvs
}

# Save the metrics to a JSON file with a timestamp
timestamp = datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
results_filename = f"/app/results/{model_name}-{timestamp}.json"
with open(results_filename, 'w') as f:
    json.dump(metrics, f, indent=4)

print(f"Results saved to {results_filename}")
